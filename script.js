const balance = document.getElementById("balance");
const money_plus = document.getElementById("money-plus");
const money_minus = document.getElementById("money-minus");
const list = document.getElementById("list");
const form = document.getElementById("form");
const text = document.getElementById("text");
const amount = document.getElementById("amount");


const dummyTransactions = [
    {
        id: 1,
        text: "Something",
        amount: 0
    },
]

let transactions = dummyTransactions;

function addTransaction(e) {
    e.preventDefault();

    if (text.value.trim() === "" || amount.value.trim() === "") {
        alert("Please add text and amount")
    } else {
        const transaction = {
            id: generateID(),
            text: text.value,
            amount: Number(amount.value)
        }

        transactions.push(transaction);

        addTransactionToDOM(transaction);

        updateValue();

        text.value = "";
        amount.value = ""
    }
}

// generate id
function generateID(){
    return Math.floor(Math.random() * 10000000000000);
}


// Add transaction to DOM
function addTransactionToDOM(transaction) {
    // get sign
    const sign = transaction.amount < 0 ? '-' : '+';

    const item = document.createElement('li');

    //  Add class based on value
    item.classList.add(transaction.amount < 0 ? 'minus' : "plus");
    item.innerHTML = `
        ${transaction.text} 
        <span>${sign}${Math.abs(transaction.amount)}</span>
        <button class="delete-btn" onclick="removeTransaction(${transaction.id})" >x</button>
        `;

    list.appendChild(item);
}

// update balance, income, expence
function updateValue() {
    const amounts = transactions.map(transaction => transaction.amount);

    const total = amounts.reduce((acc, item)=>{
        return (acc = acc + item)})
            .toFixed(2);

    const income = amounts.filter(item => item > 0)
            .reduce((acc, item)=>(acc = acc + item), 0)
            .toFixed(2);

    const expense = (amounts
            .filter(item => {return item < 0;})
            .reduce((acc, item)=>{return(acc = acc + item)}, 0) * -1)
            .toFixed(2);

    balance.innerText = `$${total}`;
    money_plus.innerText = `$${income}`;
    money_minus.innerText = `$${expense}`;

}

function removeTransaction(id){
    transactions = transactions.filter(transaction => {
        return transaction.id !== id
    })

    init();
}

// init app
function init() {
    list.innerHTML = '';
    transactions.forEach(addTransactionToDOM)
    updateValue();
}

init();

form.addEventListener("submit", addTransaction);